# Meccanica
Ciao!
Questa è la repository dedicata alla Laurea Triennale e Magistrale di Ingegneria Meccanica (Polimi) gestita dal [PoliNetwork](https://polinetwork.org)

Vi troverete materiale riguardante i vari corsi e volendo potete caricarlo anche voi attraverso il bot telegram [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

**Triennale**
 * Primo anno: Metallurgia & InformaticaB https://gitlab.com/polinetwork/meccanica1y
 * Primo anno: altri corsi https://gitlab.com/polinetwork/aes1y
 * Secondo anno https://gitlab.com/polinetwork/meccanica2y
 * Terzo anno https://gitlab.com/polinetwork/meccanica3y


**Magistrale Meccanica**
-> Per creare le repository della magistrale in aeronautica scrivete al [responsabile admin di aerospaziale](https://polinetwork.github.org/about_us)


Per qualunque info scrivete al [responsabile admin di aerospaziale](https://polinetwork.org)


 -----------------

Hello everyone this is the repository of BSc and MSc in Mechanical Engineering (Polimi) reserved by [PoliNetwork](https://polinetwork.org)

You will find and can upload the material for the courses (Notes, Exercises, ...).
 To upload files use the Telegram Bot [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

**Bachelor**
 * First year Metallurgia & Informatica B https://gitlab.com/polinetwork/meccanica1y
 * First year other courses https://gitlab.com/polinetwork/aes1y
 * Second year https://gitlab.com/polinetwork/meccanica2y
 * Third year https://gitlab.com/polinetwork/meccanica3y


**MSc Mechanical**
-> To create a repository for aeronautical Engineering contact [aerospace head admin](https://polinetwork.org/about_us)


For any query contact [aerospace head admin](https://polinetwork.org/about_us)

**ATTENTION**: You mustn't upload any copyright protected or in any other author protection form cover file on this repository or any of the ones associated to it.
